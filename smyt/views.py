#coding: utf-8
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from smyt.utils import *
from smyt.models import *
from smyt.serializers import *


class ModelEntries(generics.ListCreateAPIView):
    queryset = DynamicModel.objects.all()

    def get_serializer_class(self):
        context = self.get_renderer_context()
        DynamicSerializer.Meta.model = loading.get_model('smyt', context[u'kwargs'][u'model_name'])
        return DynamicSerializer

    def get_queryset(self):
        context = self.get_renderer_context()
        return loading.get_model('smyt', context[u'kwargs'][u'model_name']).objects.all()


class EntryDetail(generics.RetrieveUpdateDestroyAPIView):
    def get_model(self):
        self.context = self.get_parser_context(self.request)
        return loading.get_model('smyt', self.context[u'kwargs']['model_name'])

    def get_queryset(self):
        model = self.get_model()
        return model.objects.all()

    def get_object(self):
        queryset = self.get_queryset()
        return queryset.get(pk=int(self.context[u'kwargs']['id']))

    def get_serializer_class(self):
        DynamicSerializer.Meta.model = self.get_model()
        return DynamicSerializer

    def destroy(self, request, model_name, id, **kwargs):
        model = loading.get_model('smyt', model_name).objects.get(pk=id)
        model.delete()
        return Response(status=status.HTTP_200_OK)

    def retrieve(self, request, model_name, id, **kwargs):
        model = loading.get_model('smyt', model_name)
        DynamicSerializer.Meta.model = model
        serializer = DynamicSerializer(model.objects.get(pk=id))
        return Response(serializer.data)


class ModelDetail(generics.RetrieveAPIView):
    serializer_class = ModelDetailSerializer

    def retrieve(self, request, model_name, **kwargs):
        instance = DynamicModel.objects.get(name=model_name)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class ModelList(APIView):
    def get(self, request, format=None):
        """
        Показываем список всех моделей.
        """
        serializer = ModelSerializer(DynamicModel.objects.all(), many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        Для создания новой модели.
        """
        data = request.data
        try:
            name, fields, verbose_name = data['name'], data['fields'], data['title']
        except KeyError:
            return Response({'detail': ''}, status=status.HTTP_400_BAD_REQUEST)
        try:
            model = loading.get_model('test_dynamicmodel', name)
        except LookupError:
            try:
                create_model(name=name, fields=fields, verbose_name=verbose_name, save=True)
            except:
                return Response({'detail': 'AJAX syntax error!'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)