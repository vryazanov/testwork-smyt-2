#coding: utf-8
from rest_framework import serializers
from django.contrib.auth.models import User
from django.db.models import loading
from smyt.models import *


class DynamicSerializer(serializers.ModelSerializer):
    class Meta:
        pass


class FieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = DynamicField
        fields = ('name', 'title', 'field_type')


class ModelSerializer(serializers.ModelSerializer):
    fields = FieldSerializer(many=True, read_only=True)

    class Meta:
        model = DynamicModel
        fields = ('name', 'title', 'fields')


class ModelDetailSerializer(serializers.ModelSerializer):
    fields = FieldSerializer(
        many=True,
        read_only=True
    )

    class Meta:
        model = DynamicModel
        fields = ('name', 'title', 'fields')

