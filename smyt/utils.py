# coding: utf-8
from smyt.models import *
from django.db.models import loading
from django.core.urlresolvers import clear_url_caches
from django.core.management import call_command
from django.utils.importlib import import_module
from django.contrib import admin
from datetime import datetime
import logging
from smyt.settings import local_settings as settings


logger = logging.getLogger('django')


def load_dynamic_models():
    logger.info('load_dynamic_models() execute. settings.MODULES_LOADER = %s' % settings.MODULES_LOADER)
    if settings.MODULES_LOADER:
        return
    models = DynamicModel.objects.all()
    for model in models:
        fields = DynamicField.objects.filter(model=model)
        field_list = [[x.name, x.title, x.field_type] for x in fields]
        create_model(model.name, fields=field_list, verbose_name=model.title)

    settings.MODULES_LOADER = True

    clear_url_caches()
    reload(import_module(settings.ROOT_URLCONF))

def create_model(name, fields=None, save=False, verbose_name='Dynamic model'):
    class Meta:
        pass

    class Admin(admin.ModelAdmin):
        pass

    list_display = [x[0] for x in fields]

    if save:
        dmodel = DynamicModel(name=name, title=verbose_name)
        dmodel.save()

    setattr(Meta, 'app_label', 'smyt')
    setattr(Meta, 'verbose_name', verbose_name)
    setattr(Meta, 'verbose_name_plural', verbose_name)
    setattr(Admin, 'list_display', list_display)

    attrs = {'__module__': 'smyt.models', 'Meta': Meta}

    for field_name, field_title, field_type in fields:
        print field_name
        if field_type == 'CHAR':
            attrs[field_name] = models.CharField(max_length=100, default='Default value')
        elif field_type == 'INT':
            attrs[field_name] = models.IntegerField(default=0)
        elif field_type == 'DATE':
            attrs[field_name] = models.DateField()
        else:
            raise Exception
        if save:
            dfield = DynamicField(model=dmodel, name=field_name, title=field_title, field_type=field_type)
            dfield.save()

    try:
        model = loading.get_model('smyt', name)
    except LookupError:
        model = type(str(name), (models.Model,), attrs)
        admin.site.register(model, Admin)
        if save:
            call_command('migrate', interactive=True)

        clear_url_caches()
        reload(import_module(settings.ROOT_URLCONF))

    return model