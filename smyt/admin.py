#coding: utf-8
from django.contrib import admin
from smyt.models import *


class DynamicModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'title']


class DynamicFieldAdmin(admin.ModelAdmin):
    list_display = ['name', 'title', 'field_type']


admin.site.register(DynamicModel, DynamicModelAdmin)
admin.site.register(DynamicField, DynamicFieldAdmin)
