from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from smyt.utils import load_dynamic_models
from smyt.views import *

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^api/models/(?P<model_name>.+)/(?P<id>\d+)/$', EntryDetail.as_view()),
    url(r'^api/models/(?P<model_name>.+)/list$', ModelEntries.as_view()),
    url(r'^api/models/(?P<model_name>.+)/$', ModelDetail.as_view()),
    url(r'^api/models/$', ModelList.as_view()),
)

load_dynamic_models()
