#coding: utf-8
from django.db import models


class DynamicModel(models.Model):
    name = models.CharField(max_length=20)
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name + ' ' + self.title


class DynamicField(models.Model):
    model = models.ForeignKey(DynamicModel, related_name='fields')
    name = models.CharField(max_length=20)
    title = models.CharField(max_length=100)
    field_type = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name + ' ' + self.title