/**
 * Created by vryazanov on 19.03.15.
 */

angular.module('smyt', ['ui.router', 'ngResource', 'ngGrid']);

angular.module('smyt')
    .config(function($stateProvider, $urlRouterProvider, $httpProvider, $resourceProvider) {

        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

        $resourceProvider.defaults.stripTrailingSlashes = false;

        $urlRouterProvider.otherwise("/index/create");

        $stateProvider
            .state('index', {
                url: "/index",
                templateUrl: "/static/app/main.html"
            })
            .state('index.create', {
                url: "/create",
                controller: 'CreateController',
                templateUrl: "/static/app/create.html"
            })
            .state('index.model', {
                url: "/:model",
                controller: 'ViewModelController',
                templateUrl: "/static/app/model.html"
            });
    });

angular.module('smyt')
    .filter('toList', function() {
        return function(value) {
            //console.log(len);
            var res =  {
                values: [],
                names: [],
                id: null
            };
            var i = 0
            for (var key in value) {
                if (value[key][0] == '$') continue;
                if (key == "id") {
                    res.id = value[key];
                } else {
                    res.names.push(key);
                    res.values.push(value[key]);
                };
                //if (i = len) break;
            };
            console.log(res);
            return res;
    }});

angular.module('smyt')
    .controller('ListController', function($rootScope, $scope, $resource) {
        $scope.loadModels = function() {
            $scope.models = $resource('/api/models/').query();
        };

        $rootScope.$on('modelCreated', function() {
            $scope.loadModels();
        });
    });

angular.module('smyt')
    .controller('CreateController', function($rootScope, $scope, $resource, $state) {

        var res = $resource('/api/models/');

        $scope.rows = [[null, null, "CHAR"]];

        $scope.loadModels = function() {
            $scope.models = res.query();
        };

        $scope.addRow = function() {
            $scope.rows.push([null, null, "CHAR"]);
        };

        $scope.removeRow = function(id) {
            $scope.rows.splice(id, 1);
        }

        $scope.createModel = function() {
            var model = new res();
            model.name = $scope.name;
            model.title = $scope.title;
            model.fields = $scope.rows;
            model.$save().then(function(res) {
                alert('Модель создана!');
                $rootScope.$broadcast('modelCreated');
                $state.go('index.model', {model: model.name});
            });
        }
    });

angular.module('smyt')
    .controller('ViewModelController', function($scope, $resource, $stateParams, $http){
        var res = $resource('/api/models/:model/:id', null, {update: { method:'PUT' }, delete: {method: 'DELETE'}});

        $scope.selectedItems = [];
        $scope.columnDefs = [{field: "id", visible: false}];

        $scope.model = res.get({model: $stateParams.model}, function() {
            var editTemplates = {
                CHAR: '<input type="text" ng-input="COL_FIELD" ng-model="COL_FIELD">',
                INT: '<input type="number" ng-input="COL_FIELD" ng-model="COL_FIELD">',
                DATE: '<input datepicker ng-model="COL_FIELD" type="text">'
            }
            for (var i = 0; i < $scope.model.fields.length; i++)
                $scope.columnDefs.push({
                    field: $scope.model.fields[i].name,
                    displayName: $scope.model.fields[i].title,
                    editableCellTemplate: editTemplates[$scope.model.fields[i].field_type]
                });
        });

        $scope.refresh = function() {
            $http.get('/api/models/' + $stateParams.model + '/list').success(function(data){
                $scope.data = data;
            });
        };

        $scope.refresh();

        $scope.gridOptions = {
            data: 'data',
            showFooter: true,
            enableCellSelection: true,
            enableCellEdit: true,
            showSelectionCheckbox: true,
            selectedItems: $scope.selectedItems,
            columnDefs: 'columnDefs'
        };

        $scope.remove = function() {
            angular.forEach($scope.selectedItems, function(rowItem) {
                var id = rowItem.id;
                res.delete({model: $stateParams.model, id:id});
                $scope.data.splice($scope.data.indexOf(rowItem),1);
            });
        };

        $scope.add = function() {
            var default_values = {
                'INT': 0,
                'CHAR': "default value",
                'DATE': "1970-01-01"
            };
            var entry = {};

            for(var i = 0; i < $scope.model.fields.length; i++) {
                console.log($scope.model.fields[i].name);
                entry[$scope.model.fields[i].name] = default_values[$scope.model.fields[i].field_type];
            };
            console.log(entry);
            $http.post('/api/models/' + $stateParams.model + '/list', entry).success(function(resp) {
                $scope.refresh();
            });
        };

        $scope.$on('ngGridEventEndCellEdit', function(evt){
            var entry = evt.targetScope.row.entity;
            res.update({model: $stateParams.model, id: entry.id}, entry);
        });
    });

angular.module('smyt')
    .directive('datepicker', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                $(element).datepicker({
                    dateFormat:'yy-mm-dd',
                    onSelect: function (date) {
                        ngModelCtrl.$setViewValue(date);
                        scope.$emit('ngGridEventEndCellEdit');
                    }
                });
            }
        };
});

